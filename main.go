package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
)

type word struct {
	str1, str2 string
	count      int
	deleted    bool
}

func (w *word) getScore() float64 {
	if w.count == 0 {
		panic("this should not happen")
	}
	return 1 / float64(w.count)
}

var words = make([]*word, 0)
var notLearned = make([]*word, 0)
var learned = make([]*word, 0)
var fifo = make([]*word, 0)

func main() {
	if len(os.Args) != 2 {
		log.Fatalln("Too much or too many arguments: want filepath")
	}
	data, err := os.ReadFile(os.Args[1])
	if err != nil {
		log.Fatalln(err)
	}
	str := string(data)
	lines := strings.Split(str, "\n")

	for i, l := range lines {
		if l == "" {
			continue
		}
		w := getWordFromLine(l)
		if w == nil {
			log.Fatalln("Wrong format on the line: ", i)
		}
		words = append(words, w)
		if w.count == 0 {
			notLearned = append(notLearned, w)
		} else {
			learned = append(learned, w)
		}
	}
	fmt.Println("Sucessfully loaded ", len(words))
	if len(words) == 0 {
		log.Fatalln("Terminating")
	}

	loadToFifo()

	reader := bufio.NewReader(os.Stdin)
	for {
		w := fifo[0]
		fmt.Print("(" + strconv.Itoa(w.count) + ") " + w.str1 + " ")
		reader.ReadString('\n')
		fmt.Print(w.str2 + " (y/d)?: ")
		text, _ := reader.ReadString('\n')

		switch text {
		case "y\n":
			w.count += 1
			learned = append(learned, w)
		case "d\n":
			w.deleted = true
		case "\n":
			w.count = 0
			fifo = append(fifo, w)
		default:
			log.Fatalln("Wrong input terminating")
		}

		fifo = fifo[1:]
		loadToFifo()
		saveToFile(os.Args[1])
		fmt.Println()
	}
}

func saveToFile(path string) {
	file, _ := os.OpenFile(path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	datawriter := bufio.NewWriter(file)
	for _, w := range words {
		if !w.deleted {
			_, _ = datawriter.WriteString(w.str1 + ";" + w.str2 + ";" + strconv.Itoa(w.count) + "\n")
		}
	}
	datawriter.Flush()

	file.Close()
}

func loadToFifo() {
	num := 10
	prob := 0.9

	var w *word
	for len(fifo) < num {
		r := rand.Float64()
		if r < prob {
			w = getFromNotLearned()
			if w == nil {
				w = getFromLearned()
			}
		} else {
			w = getFromLearned()
			if w == nil {
				w = getFromNotLearned()
			}
		}
		if w == nil {
			break
		}
		fifo = append(fifo, w)
	}
}

func getFromNotLearned() *word {
	if len(notLearned) == 0 {
		return nil
	}
	i := rand.Intn(len(notLearned))
	w := notLearned[i]
	notLearned = append(notLearned[:i], notLearned[i+1:]...)
	return w
}

func getFromLearned() *word {
	if len(learned) == 0 {
		return nil
	}
	sum := .0
	for _, w := range learned {
		sum += w.getScore()
	}
	pdf := make([]float64, 0)
	for _, w := range learned {
		pdf = append(pdf, w.getScore()/sum)
	}
	i := sample(pdf)
	w := learned[i]
	learned = append(learned[:i], learned[i+1:]...)
	return w
}

func sample(pdf []float64) int {
	cdf := make([]float64, len(pdf))
	cdf[0] = pdf[0]
	for i := 1; i < len(pdf); i++ {
		cdf[i] = cdf[i-1] + pdf[i]
	}
	r := rand.Float64()
	bucket := 0
	for r > cdf[bucket] {
		bucket++
	}
	return bucket
}

func getWordFromLine(line string) *word {
	splited := strings.Split(line, ";")
	if l := len(splited); l != 2 && l != 3 {
		return nil
	}
	str1 := strings.TrimSpace(splited[0])
	str2 := strings.TrimSpace(splited[1])
	if str1 == "" || str2 == "" {
		return nil
	}
	w := &word{
		str1:    str1,
		str2:    str2,
		deleted: false,
	}
	if len(splited) == 3 {
		num, err := strconv.Atoi(splited[2])
		if err != nil {
			return nil
		}
		w.count = num
	}
	return w
}
